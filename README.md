**Deprecated: The functionality is not integrated in the [Mathezirkel App](https://gitlab.com/mathezirkel/mathezirkel-app/exports).**

# Voraussetzungen

* python 3
* gphoto2 (oder etwas äquivalentes)
* latex
* gThumb zum Schneiden und Speichern/Umbenennen

# Ablauf

## Foto machen

```shell
$ gphoto2 -F 1 --capture-image-and-download
```

* Dateiname ist immer der gleiche (capt0001.jpg oder so)
* Danach Bild in gThumb öffnen und zurechtschneiden.
* Die Standardeinstellungen von gThumb machen das Bild auch gleich "ok" groß, das waren dann meistens ein paar Hundert Kilobyte
* Ursprüngliche Datei capt0001.jpg löschen, weil gphoto2 sonst fragt ob es die überschreiben soll. (Das wird automatisch durch den nächsten Schritt erledigt.)

## Foto umbenennen

* Eine Datei der Form
  ```zsh
  _participants=(
      'vornamenachname1'
      'vornamenachname2'
  )
  ```
  erstellen.
* Die Dateipfade und das Format in der `zsh-completion-participants` anpassen.
* Die `zsh-completion-participants` in dem Ordner, in dem die Bilder erstellt werden, einbinden.
* Durch den Befehl
  ```shell
  $ m capt0001.jpg vornamenachname
  ```
  wird das Bild dann automatisch umbenannt und passend verschoben. Der Befehl unterstützt Autovervollständigung.

## Fotos runterrechnen

War bei mir nicht nötig.

```shell
$ convert input.jpg -quality 80 output.jpg
```

## Skript aufrufen um Latex-Datei zu erzeugen

* Pfade und eventuell Optionen in Python-Skript anpassen
* Windows-Pfade sollten direkt funktionieren, eventuell musst du den String literal machen (r"") oder die Backslashes durch Forwardshlashes ersetzen

```shell
$ python create-kinder-latex.py
```

## Latex

```shell
$ pdflatex kinder-skript-output.tex
```
* Dauert 5s

## License

This project is licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.txt), see [LICENSE](LICENSE).
